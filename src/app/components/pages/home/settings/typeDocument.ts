export const typeDocument = [
    {type:"cc" , name:"cedula de ciudadania"},
    {type:"ti" , name:"tarjeta de identidad"},
    {type:"tp" , name:"pasaporte"},
    {type:"rc" , name:"registro civil"},
    {type:"ce" , name:"cedula de extranjeria"},
    {type:"ci" , name:"carne de identidad"},
    {type:"dni" , name:"documento unico de identidad"}
];