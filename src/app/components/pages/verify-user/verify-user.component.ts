import { Component} from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.css']
})
export class VerifyUserComponent  {

  constructor(private auth:AuthService,
    private userService:UserService,
    private router:Router) { 
  }

  home(){
    this.router.navigateByUrl('/home');
  }

  logout(){
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

  resendEmail(){
    this.userService.resendEmail().subscribe(
      data=>console.log(data)
    )
  }

}
