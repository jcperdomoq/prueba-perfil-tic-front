import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { async } from '@angular/core/testing';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class VerifyUserGuard implements CanActivate {

  constructor(private userService:UserService,
    private router:Router){
  }
  
  async canActivate(): Promise<boolean> {
    const user = await this.userService.getUser();
    if(user){
      return true;
    }else{
      this.router.navigateByUrl('verify');
      return false;
    }
  }
  
}
